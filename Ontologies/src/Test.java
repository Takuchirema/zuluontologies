
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.profiles.OWL2DLProfile;
import org.semanticweb.owlapi.profiles.OWLProfileReport;
import org.semanticweb.owlapi.profiles.OWLProfileViolation;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.util.OWLClassExpressionVisitorAdapter;
import org.semanticweb.owlapi.util.OWLOntologyWalker;
import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitorEx;

public class Test {
	
	private static String ontology_path="C:/Users/User/Desktop/computer_science/VacWork/June2016/ontologies/ZuluAfricanWildlife.owl";
	private static String resources_path="C:/Users/User/Desktop/computer_science/VacWork/June2016/resources";
	
	private static HashMap<String,String> savedNouns=new HashMap<String,String>();
	
	private static HashMap<String,NounClass> nounClasses = new HashMap<String,NounClass>();
	
	private static HashMap<Character,ArrayList<String>> verbs = new HashMap<Character,ArrayList<String> >();
	
	private static ArrayList<String> adjectives = new ArrayList<String>();
	
	public static void main (String[] args) throws OWLOntologyCreationException, IOException{
		
		OWLOntology o = loadOntology();
		
		//loadNounClasses();
		
		//showOntologyClasses(o);
		
		//walkOntology(o);
		
		//restrictionVisitor(o);
		
		analyseAxioms(o);
		
	}
	
	/** This methods loads all saved verbs into the ArrayList verbs */
	public static HashMap<Character,ArrayList<String>> loadVerbs() throws IOException{
		
		BufferedReader bf = new BufferedReader(new FileReader(resources_path+"/Verbs.txt"));
		
		String line;
		
		char letter = 'z';
		while ( (line = bf.readLine()) != null){
			
			if (line.charAt(0)==letter){
				verbs.get(letter).add(line);
			}else{
				letter = line.charAt(0);
				ArrayList<String> verbList = new ArrayList<String>();
				verbList.add(line);
				verbs.put(letter,verbList);
			}
			//System.out.println("verb: "+line);
		}
		
		return verbs;
	}
	
	/** This methods loads all saved nouns into the HashMap savedNouns
	 * The key is the noun
	 * The value is the noun class */
	public static HashMap<String,String> loadSavedNouns() throws IOException{
		
		BufferedReader bf = new BufferedReader(new FileReader(resources_path+"/Nouns.txt"));
		
		//the first line has the headings
		bf.readLine();
		
		String line;
		
		while ( (line = bf.readLine()) != null){
			
			String[] nc = line.split(",");
			//System.out.println("1st Item: "+nc[0]+Arrays.toString(nc));
			
			savedNouns.put(nc[0],nc[1]);
		}
		
		return savedNouns;
		
	}
	
	/** This methods loads all adjectives into the ArrayList adjectives */
	public static ArrayList<String> loadAdjectives() throws IOException{
		
		BufferedReader bf = new BufferedReader(new FileReader(resources_path+"/Adjectives.txt"));
		
		String line;
		
		while ( (line = bf.readLine()) != null){
			
			adjectives.add(line);
		}
		
		return adjectives;
		
	}
	
	/** This method loads the noun classes into the ArrayList nounClasses
	 * They are as defined in the NounClasses.tsv */
	public static HashMap<String,NounClass> loadNounClasses() throws IOException{
		
		BufferedReader bf = new BufferedReader(new FileReader(resources_path+"/NounClasses.tsv"));
		
		//the first line has the headings
		bf.readLine();
		
		String line;
		
		while ( (line = bf.readLine()) != null){
			
			line = line.replace("-","");
			String[] nc = line.split("\\t");
			//System.out.println("2nd Item: "+nc[1]+Arrays.toString(nc));
			
			NounClass nounClass = new NounClass (nc[0]);
			nounClass.setVariables(nc);
			
			nounClasses.put(nc[0],nounClass);
		}
		
		return nounClasses;
	}
	
	public static OWLOntology loadOntology() throws OWLOntologyCreationException{
		File file = new File(ontology_path);
		
		OWLOntologyManager m = OWLManager.createOWLOntologyManager();
		
		OWLOntology o = m.loadOntologyFromOntologyDocument(file);
		
		return o;
	}
	
	public static void analyseAxioms(OWLOntology o){
    	// Iterate over the axioms of the ontology. There are more than just the subclass
        // axiom, because the class declarations are also axioms.  All in all, there are
        // four:  the subclass axiom and three declarations of named classes.
        System.out.println( "== All Axioms: ==" );
        for ( final OWLAxiom axiom : o.getAxioms() ) {
            System.out.println("#########################\n"+axiom);
            //System.out.println( axiom );
            String currentAxiom = axiom.toString();
            
            AxiomAnalyser a = new AxiomAnalyser(currentAxiom);
            
            try {
				ArrayList<Word> words = a.analyse(currentAxiom);
				//printWords(words);
				
				WordsPattern wordsPattern = new WordsPattern(words);
				wordsPattern.verbalise();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            System.out.println(" -------------------------- ");
             
        }
    }
	
	public static void printWords(ArrayList<Word> words){
		
		for(Word word: words){
			System.out.println("word: "+word.getWord()+" type: "+word.getType());
		}
		
	}
	
	public static void walkOntology(OWLOntology o){
		OWLOntologyWalker walker = new OWLOntologyWalker(Collections.singleton(o));
		
		OWLOntologyWalkerVisitorEx<Object> visitor = new OWLOntologyWalkerVisitorEx<Object>(walker) {

            @Override
            public Object visit(OWLObjectSomeValuesFrom desc) {
                // Print out the restriction
                System.out.println("\n###"+desc);
                // Print out the axiom where the restriction is used
                String currentAxiom = getCurrentAxiom().toString();
                System.out.println("***" + currentAxiom);
                // We don't need to return anything here.
                AxiomAnalyser a = new AxiomAnalyser(currentAxiom);
                
                try {
					a.analyse(currentAxiom);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                 
                return "";
            }
        };
        // Now ask the walker to walk over the ontology structure using our
        // visitor instance.
        walker.walkStructure(visitor);
	}
	
	/** Implementation of the Levenshtein distance between 2 words
	 *  Based on the definition given in the Wikipedia article*/
	public static int distance(String a, String b) {
        a = a.toLowerCase();
        b = b.toLowerCase();
        // i == 0
        int [] costs = new int [b.length() + 1];
        for (int j = 0; j < costs.length; j++)
            costs[j] = j;
        for (int i = 1; i <= a.length(); i++) {
            // j == 0; nw = lev(i - 1, j)
            costs[0] = i;
            int nw = i - 1;
            for (int j = 1; j <= b.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }
        return costs[b.length()];
    }
	
	public static void showOntologyClasses(OWLOntology o){
		
		OWL2DLProfile profile = new OWL2DLProfile();
		OWLProfileReport report = profile.checkOntology(o);
		
		System.out.println("got here!!");
		
		for (OWLClass cls : o.getClassesInSignature()){
			System.out.println(cls);
		}
	}
	
	public static void restrictionVisitor(OWLOntology o){
		
		RestrictionVisitor restrictionVisitor = new RestrictionVisitor(Collections.singleton(o));
		
		for (OWLClass cls : o.getClassesInSignature()){
			for (OWLSubClassOfAxiom ax : o.getSubClassAxiomsForSubClass(cls)) {
				ax.getSuperClass().accept(restrictionVisitor);
		    }
			
			// Ask our superclass to accept a visit from the RestrictionVisitor
	        // - if it is an existential restiction then our restriction visitor
	        // will answer it - if not our visitor will ignore it
	        // Our RestrictionVisitor has now collected all of the properties that
	        // have been restricted in existential restrictions - print them out.
	        System.out.println("Restricted properties for " + cls + ": " + 
	        					restrictionVisitor.getRestrictedProperties().size());
	        
	        for (OWLObjectPropertyExpression prop : restrictionVisitor.getRestrictedProperties()) {
	        	System.out.println(" " + prop);
	        }
		}
		
	}
	
	/**
     * Visits existential restrictions and collects the properties which are
     * restricted.
     */
    private static class RestrictionVisitor extends OWLClassExpressionVisitorAdapter {

        private final Set<OWLClass> processedClasses;
        private final Set<OWLOntology> onts;
        private final Set<OWLObjectPropertyExpression> properties = new HashSet<OWLObjectPropertyExpression>();
        
        RestrictionVisitor(Set<OWLOntology> onts) {
            processedClasses = new HashSet<OWLClass>();
            this.onts = onts;
        }

        @Override
        public void visit(OWLClass ce) {
            if (!processedClasses.contains(ce)) {
                // If we are processing inherited restrictions then we
                // recursively visit named supers. Note that we need to keep
                // track of the classes that we have processed so that we don't
                // get caught out by cycles in the taxonomy
                processedClasses.add(ce);
                for (OWLOntology ont : onts) {
                    for (OWLSubClassOfAxiom ax : ont.getSubClassAxiomsForSubClass(ce)) {
                        ax.getSuperClass().accept(this);
                    }
                }
            }
        }

        @Override
        public void visit(OWLObjectSomeValuesFrom desc) {
            // This method gets called when a class expression is an existential
            // (someValuesFrom) restriction and it asks us to visit it
        	properties.add(desc.getProperty());
        }
        
        public Set<OWLObjectPropertyExpression> getRestrictedProperties() { return properties; }
    }
    
    public static HashMap<String,NounClass> getNounClasses(){
    	return nounClasses;
    }

}

