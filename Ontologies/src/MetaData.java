
public class MetaData {
	
	private MetaType metaType;
	
	/* Common to All */
	private int id;
	
	/* Role, Entity Type */
	private String name;
	
	/* Role */
	private String tense;
	private String lCase;
	private boolean hasPre=false;
	private String pre;
	
	/* Entity Type */
	private String gender;
	private String nounClass;
	private int grammNumber;
	
	/* Reading Pattern */
	private String pattern;
	private String language;
	
	/* Axiom Type */
	private boolean positionalist;
	
	/* Reading */
	private String sentence;

	
	public MetaType getMetaType() {
		return metaType;
	}

	public void setMetaType(MetaType metaType) {
		this.metaType = metaType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTense() {
		return tense;
	}

	public void setTense(String tense) {
		this.tense = tense;
	}

	public String getlCase() {
		return lCase;
	}

	public void setlCase(String lCase) {
		this.lCase = lCase;
	}

	public boolean isHasPre() {
		return hasPre;
	}

	public void setHasPre(boolean hasPre) {
		this.hasPre = hasPre;
	}

	public String getPre() {
		return pre;
	}

	public void setPre(String pre) {
		this.pre = pre;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNounClass() {
		return nounClass;
	}

	public void setNounClass(String nounClass) {
		this.nounClass = nounClass;
	}

	public int getGrammNumber() {
		return grammNumber;
	}

	public void setGrammNumber(int grammNumber) {
		this.grammNumber = grammNumber;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public boolean isPositionalist() {
		return positionalist;
	}

	public void setPositionalist(boolean positionalist) {
		this.positionalist = positionalist;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}
	
	
}
