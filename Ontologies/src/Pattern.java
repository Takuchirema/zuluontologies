
public enum Pattern {
	
	/** allQPronoun:
	 * Takes the inclusive quantitative pronoun of the succeeding noun
	 * e.g. Zo-nke izindlovu
	 *  
	 * oneQPronoun:
	 * Takes the exclusive quantitative pronoun of the last noun in the pattern
	 * 
	 * plural:
	 * takes the plural concord of the noun
	 * 
	 * sConcord:
	 * Takes the subject concord of the noun
	 * 
	 * root:
	 * takes the root of the noun
	 * 
	 * atLeast inserts the word 'okungengenani' meaning 'at least' next in the pattern*/
	
	allQPronoun,
	oneQPronoun,
	plural,
	sConcord,
	root,
	atLeast,
	complement,
	intersection,
	union,
	only,
	unknown;

}
