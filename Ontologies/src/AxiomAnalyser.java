import java.io.IOException;
import java.util.ArrayList;


public class AxiomAnalyser {
	
	String theAxiom;
	
	public AxiomAnalyser(String axiom){
		this.theAxiom=axiom;
	}
	
	public ArrayList<Word> analyse(String axiom) throws IOException{
		ArrayList<Word> pattern = new ArrayList<Word>();
		
		String axiomType= axiom.substring(0,axiom.indexOf("(") );
		//system.out.println("axiom type: "+axiomType);
		String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
		//system.out.println("inner axiom: "+innerAxiom);
		
		if (axiomType.equals("SubClassOf")){
			pattern = subClassOf(innerAxiom);
		}else if (axiomType.equals("ObjectSomeValuesFrom")){
			pattern = objectSomeValuesFrom(innerAxiom);
		}else if (axiomType.equals("ObjectIntersectionOf")){
			pattern = objectIntersectionOf(innerAxiom);
		}else if (axiomType.equals("EquivalentClasses")){
			pattern = EquivalentClasses(innerAxiom);
		}else if (axiomType.equals("ObjectUnionOf")){
			pattern = objectUnionOf(innerAxiom);
		}else if (axiomType.equals("ObjectAllValuesFrom")){
			pattern = objectAllValuesFrom(innerAxiom);
		}else if (axiomType.equals("ObjectComplementOf")){
			pattern = objectComplementOf(innerAxiom);
		}
		
		return pattern;
	}
	
	public ArrayList<Word> analyse(String axiomType,String innerAxiom) throws IOException{
		
		ArrayList<Word> pattern = new ArrayList<Word>(); 
		
		if (axiomType.equals("SubClassOf")){
			pattern = subClassOf(innerAxiom);
		}else if (axiomType.equals("ObjectSomeValuesFrom")){
			pattern = objectSomeValuesFrom(innerAxiom);
		}else if (axiomType.equals("ObjectIntersectionOf")){
			pattern = objectIntersectionOf(innerAxiom);
		}else if (axiomType.equals("EquivalentClasses")){
			pattern = EquivalentClasses(innerAxiom);
		}else if (axiomType.equals("ObjectUnionOf")){
			pattern = objectUnionOf(innerAxiom);
		}else if (axiomType.equals("ObjectAllValuesFrom")){
			pattern = objectAllValuesFrom(innerAxiom);
		}else if (axiomType.equals("ObjectComplementOf")){
			pattern = objectComplementOf(innerAxiom);
		}
		
		return pattern;
	}

	public ArrayList<Word> subClassOf(String values) throws IOException{
		
		ArrayList<Word> pattern = new ArrayList<Word>(); 
		
		String subject = values.substring(0,values.indexOf(" "));
		//system.out.println("subject: "+subject);
		String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
		//system.out.println("subjectClass: "+subjectClass);
		
		String axiom=values.substring(values.indexOf(" ")+1,values.length());
		
		/* prepare the words and pattern values here */
		//Split the subject in case it is two words to explain it
		String[] s = subjectClass.split("-");
		
		for (String i: s){
			Word word = new Word(i);
			word.deriveType();
			
			MetaData metaData = new MetaData();
			metaData.setMetaType(MetaType.entityType);
			word.setMetaData(metaData);
			
			pattern.add(word);
		}
		
		String axiomType="";
		if (axiom.charAt(0)!='<'){
			axiomType= axiom.substring(0,axiom.indexOf("(") );
			
			//system.out.println("axiom type: "+axiomType);
			String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
			//system.out.println("inner axiom: "+innerAxiom);
			
			pattern.addAll(analyse(axiomType,innerAxiom));
			
		}else{
			subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
			s = subjectClass.split("-");
			//system.out.println("subjectClass2: "+subjectClass);
			
			for (String i: s){
				Word word = new Word(i);
				word.deriveType();
				pattern.add(word);
			}
		}
		
		return pattern;
	}
	
	
	public ArrayList<Word> objectComplementOf(String values) throws IOException{
		
		ArrayList<Word> pattern = new ArrayList<Word>();
		
		//check if the intersections are classes or axioms
		char check = values.charAt(values.charAt(0));
		//system.out.println("check char: "+check);
		
		if (check== '<'){
			String subject = values.substring(0,values.indexOf(" "));
			//system.out.println("subject: "+subject);
			String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
			//system.out.println("subjectClass: "+subjectClass);
			
			String axiom=values.substring(values.indexOf(" ")+1,values.length());
			
			/* prepare the words and pattern values here */
			//Split the subject in case it is two words to explain it
			String[] s = subjectClass.split("-");
			
			/* Insert the -ona first e.g. for aku-y-ona */
			Word ona = new Word("ona");
			ona.setStem("ona");
			ona.setPattern(Pattern.complement);
			pattern.add(ona);
			/* **************************************** */
			
			for (String i: s){
				Word word = new Word(i);
				word.deriveType();
				pattern.add(word);
			}
			
			String axiomType="";
			
			if (axiom.charAt(0)!='<'){
				axiomType= axiom.substring(0,axiom.indexOf("(") );
				
				//system.out.println("axiom type: "+axiomType);
				String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
				//system.out.println("inner axiom: "+innerAxiom);
				
				pattern.addAll(analyse(axiomType,innerAxiom));
				
			}else{ //Then its a class not an inner axiom
				subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
				s = subjectClass.split("-");
				//system.out.println("subjectClass2: "+subjectClass);
				
				for (String i: s){
					Word word = new Word(i);
					word.deriveType();
					pattern.add(word);
				}
			}
			
		}else{
			//first axiom
			String axiom=checkAxioms(pattern,values);
			//system.out.println("******* First round axiom: "+axiom);
			
			/* Insert the -ona first e.g. for aku-y-ona */
			Word ona = new Word("ona");
			ona.setStem("ona");
			ona.setPattern(Pattern.complement);
			pattern.add(ona);
			/* **************************************** */
			
			//loop:
			while (!(axiom=checkAxioms(pattern,axiom)).equals("")){
				
				/* Insert the -ona first e.g. for aku-y-ona */
				ona = new Word("ona");
				ona.setStem("ona");
				ona.setPattern(Pattern.complement);
				pattern.add(ona);
				/* **************************************** */
				
				if (axiom.charAt(0)=='<'){
					
					String subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
					String[] s = subjectClass.split("-");
					//system.out.println("subjectClass<<: "+subjectClass);
					
					for (String i: s){
						Word word = new Word(i);
						word.deriveType();
						pattern.add(word);
					}
				}
				//break loop;
			}

		}
		
		return pattern;
	}
	
	public ArrayList<Word> objectSomeValuesFrom(String values) throws IOException{
		
		ArrayList<Word> pattern = new ArrayList<Word>();
		
		String subject = values.substring(0,values.indexOf(" "));
		//system.out.println("subject: "+subject);
		String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
		//system.out.println("subjectClass: "+subjectClass);
		
		String axiom=values.substring(values.indexOf(" ")+1,values.length());
		
		/* prepare the words and pattern values here */
		//Split the subject in case it is two words to explain it
		String[] s = subjectClass.split("-");
		
		/* Insert the -nke first */
		Word nke = new Word("nke");
		nke.setStem("nke");
		nke.setPattern(Pattern.allQPronoun);
		pattern.add(nke);
		
		for (String i: s){
			Word word = new Word(i);
			word.deriveType();
			
			if (word.getType()==Type.verb){
				word.setPattern(Pattern.sConcord);
				//system.out.println("*** verb n set : "+word.getWord());
			}
			
			MetaData metaData = new MetaData();
			metaData.setMetaType(MetaType.role);
			word.setMetaData(metaData);
			
			pattern.add(word);
		}
		
		String axiomType="";
		if (axiom.charAt(0)!='<'){
			axiomType= axiom.substring(0,axiom.indexOf("(") );
			
			//system.out.println("axiom type: "+axiomType);
			String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
			//system.out.println("inner axiom: "+innerAxiom);
			
			pattern.addAll(analyse(axiomType,innerAxiom));
			
		}else{
			subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
			s = subjectClass.split("-");
			//system.out.println("subjectClass2: "+subjectClass);
			
			for (String i: s){
				Word word = new Word(i);
				word.deriveType();
				
				if (word.getType()==Type.verb){
					word.setPattern(Pattern.sConcord);
				}
				
				pattern.add(word);
			}
		}
		
		/* Insert the -dwa last */
		Word dwa = new Word("dwa");
		dwa.setStem("dwa");
		dwa.setPattern(Pattern.oneQPronoun);
		pattern.add(dwa);
		
		return pattern;
	}
	
	public ArrayList<Word> objectIntersectionOf(String values) throws IOException{
		
		ArrayList<Word> pattern = new ArrayList<Word>();
		
		//check if the intersections are classes or axioms
		char check = values.charAt(values.charAt(0));
		//system.out.println("check char: "+check);
		
		if (check== '<'){
			String subject = values.substring(0,values.indexOf(" "));
			//system.out.println("subject: "+subject);
			String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
			//system.out.println("subjectClass: "+subjectClass);
			
			String axiom=values.substring(values.indexOf(" ")+1,values.length());
			
			/* prepare the words and pattern values here */
			//Split the subject in case it is two words to explain it
			String[] s = subjectClass.split("-");
			
			for (String i: s){
				Word word = new Word(i);
				word.deriveType();
				if (word.getType()==Type.verb){
					word.setPattern(Pattern.sConcord);
				}
				pattern.add(word);
			}
			
			/* Insert the futhi at intersection */
			Word futhi = new Word("futhi");
			futhi.setStem("futhi");
			futhi.setPattern(Pattern.intersection);
			pattern.add(futhi);
			/* ******************************** */
			
			String axiomType="";
			
			if (axiom.charAt(0)!='<'){
				axiomType= axiom.substring(0,axiom.indexOf("(") );
				
				//system.out.println("axiom type: "+axiomType);
				String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
				//system.out.println("inner axiom: "+innerAxiom);
				
				pattern.addAll(analyse(axiomType,innerAxiom));
				
			}else{ //Then its a class not an inner axiom
				subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
				s = subjectClass.split("-");
				//system.out.println("subjectClass2: "+subjectClass);
				
				for (String i: s){
					Word word = new Word(i);
					word.deriveType();
					if (word.getType()==Type.verb){
						word.setPattern(Pattern.sConcord);
					}
					pattern.add(word);
				}
			}
			
		}else{
			//first axiom
			String axiom=checkAxioms(pattern,values);
			//system.out.println("******* First round axiom: "+axiom);
			
			/* Insert the futhi at intersection */
			Word futhi = new Word("futhi");
			futhi.setStem("futhi");
			futhi.setPattern(Pattern.intersection);
			pattern.add(futhi);
			/* ******************************** */
			
			//loop:
			while (!(axiom=checkAxioms(pattern,axiom)).equals("")){
				
				/* Insert the futhi at intersection */
				futhi = new Word("futhi");
				futhi.setStem("futhi");
				futhi.setPattern(Pattern.intersection);
				pattern.add(futhi);
				/* ******************************** */

				if (axiom.charAt(0)=='<'){
					
					String subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
					String[] s = subjectClass.split("-");
					//system.out.println("subjectClass2: "+subjectClass);
					
					for (String i: s){
						Word word = new Word(i);
						word.deriveType();
						if (word.getType()==Type.verb){
							word.setPattern(Pattern.sConcord);
						}
						pattern.add(word);
					}
				}
				//break loop;
			}
			
		}
		
		return pattern;
	}
	
	/** The number of brackets opened will show the number of brackets to be closed before axiom ends 
	 * e.g. OSVO(dla OIO(isilwane ihebhivo)) OSVO() . . . 
	 * For the first part 2 brackets were opened and so 2 brackets must be closed
	 * If count is 0 we know an axiom has been closed and do the procedure 
	 * @throws IOException */
	public String checkAxioms(ArrayList<Word> pattern,String values) throws IOException{
		
		String axiom2="";
		
		int bracketCount = 0;
		
		loop:
		for (int i=0;i<values.length();i++){
			if (values.charAt(i)=='(')
				bracketCount++;
			else if (values.charAt(i)==')'){
				bracketCount--;
				
				if (bracketCount ==0){
					String axiom1="";
					if (values.length()>=i){
						axiom1 = values.substring(0,i+1);
						//system.out.println("**axiom: "+axiom1);
					}else{
						axiom1 = values.substring(0,i);
						//system.out.println("##axiom: "+axiom1);
					}
					
					pattern.addAll(analyse(axiom1));
					axiom2 = values.replace(axiom1, "");
					axiom2 = axiom2.replaceAll("^\\s+", "");
					
					//system.out.println("Replaced axiom: "+axiom2);
					
					break loop;
				}
			}
		}
		
		return axiom2;
	}

	public ArrayList<Word> EquivalentClasses(String values) throws IOException{
		
		ArrayList<Word> pattern = new ArrayList<Word>();
		
		String subject = values.substring(0,values.indexOf(" "));
		//system.out.println("subject: "+subject);
		String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
		//system.out.println("subjectClass: "+subjectClass);
		
		String axiom=values.substring(values.indexOf(" ")+1,values.length());
		
		/* prepare the words and pattern values here */
		//Split the subject in case it is two words to explain it
		String[] s = subjectClass.split("-");
		
		for (String i: s){
			Word word = new Word(i);
			word.deriveType();
			pattern.add(word);
		}
		
		String axiomType="";
		if (axiom.charAt(0)!='<'){
			axiomType= axiom.substring(0,axiom.indexOf("(") );
			
			//system.out.println("axiom type: "+axiomType);
			String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
			//system.out.println("inner axiom: "+innerAxiom);
			
			pattern.addAll(analyse(axiomType,innerAxiom));
			
		}else{
			subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
			s = subjectClass.split("-");
			//system.out.println("subjectClass2: "+subjectClass);
			
			for (String i: s){
				Word word = new Word(i);
				word.deriveType();
				if (word.getType()==Type.verb){
					word.setPattern(Pattern.sConcord);
				}
				pattern.add(word);
			}
		}
		
		return pattern;
	}

	public ArrayList<Word> objectUnionOf(String values) throws IOException{
		
		ArrayList<Word> pattern = new ArrayList<Word>();
		
		//check if the intersections are classes or axioms
		char check = values.charAt(values.charAt(0));
		//system.out.println("check char: "+check);
		
		if (check== '<'){
			String subject = values.substring(0,values.indexOf(" "));
			//system.out.println("subject: "+subject);
			String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
			//system.out.println("subjectClass: "+subjectClass);
			
			String axiom=values.substring(values.indexOf(" ")+1,values.length());
			
			/* prepare the words and pattern values here */
			//Split the subject in case it is two words to explain it
			String[] s = subjectClass.split("-");
			
			for (String i: s){
				Word word = new Word(i);
				word.deriveType();
				if (word.getType()==Type.verb){
					word.setPattern(Pattern.sConcord);
				}
				pattern.add(word);
			}
			
			/* Insert the noma at intersection */
			Word noma = new Word("noma");
			noma.setStem("noma");
			noma.setPattern(Pattern.union);
			pattern.add(noma);
			/* ******************************** */
			
			String axiomType="";
			
			if (axiom.charAt(0)!='<'){
				axiomType= axiom.substring(0,axiom.indexOf("(") );
				
				//system.out.println("axiom type: "+axiomType);
				String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
				//system.out.println("inner axiom: "+innerAxiom);
				
				pattern.addAll(analyse(axiomType,innerAxiom));
				
			}else{ //Then its a class not an inner axiom
				subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
				s = subjectClass.split("-");
				//system.out.println("subjectClass2: "+subjectClass);
				
				for (String i: s){
					Word word = new Word(i);
					word.deriveType();
					if (word.getType()==Type.verb){
						word.setPattern(Pattern.sConcord);
					}
					pattern.add(word);
				}
			}
			
		}else{
			//first axiom
			String axiom=checkAxioms(pattern,values);
			//system.out.println("******* First round axiom: "+axiom);
			
			/* Insert the noma at intersection */
			Word noma = new Word("noma");
			noma.setStem("noma");
			noma.setPattern(Pattern.union);
			pattern.add(noma);
			/* ******************************** */
			
			//loop:
			while (!(axiom=checkAxioms(pattern,axiom)).equals("")){
				
				/* Insert the noma at intersection */
				noma = new Word("noma");
				noma.setStem("noma");
				noma.setPattern(Pattern.union);
				pattern.add(noma);
				/* ******************************** */
				
				if (axiom.charAt(0)=='<'){
					
					String subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
					String[] s = subjectClass.split("-");
					//system.out.println("subjectClass<<: "+subjectClass);
					
					for (String i: s){
						Word word = new Word(i);
						word.deriveType();
						if (word.getType()==Type.verb){
							word.setPattern(Pattern.sConcord);
						}
						pattern.add(word);
					}
				}
				//break loop;
			}

		}
		
		return pattern;
	}
	
	public ArrayList<Word> objectAllValuesFrom(String values) throws IOException{
		
		ArrayList<Word> pattern = new ArrayList<Word>();
		
		String subject = values.substring(0,values.indexOf(" "));
		//system.out.println("subject: "+subject);
		String subjectClass = subject.substring(subject.indexOf("#")+1,subject.length()-1);
		//system.out.println("subjectClass: "+subjectClass);
		
		String axiom=values.substring(values.indexOf(" ")+1,values.length());
		
		/* prepare the words and pattern values here */
		//Split the subject in case it is two words to explain it
		String[] s = subjectClass.split("-");
		
		/* Insert the -nke first */
		Word nke = new Word("nke");
		nke.setStem("nke");
		nke.setPattern(Pattern.allQPronoun);
		pattern.add(nke);
		
		for (String i: s){
			Word word = new Word(i);
			word.deriveType();
			
			if (word.getType()==Type.verb){
				word.setPattern(Pattern.sConcord);
				//system.out.println("*** verb n set : "+word.getWord());
			}
			
			MetaData metaData = new MetaData();
			metaData.setMetaType(MetaType.role);
			word.setMetaData(metaData);
			pattern.add(word);
		}
		
		String axiomType="";
		if (axiom.charAt(0)!='<'){
			axiomType= axiom.substring(0,axiom.indexOf("(") );
			
			//system.out.println("axiom type: "+axiomType);
			String innerAxiom = axiom.substring(axiom.indexOf("(")+1,axiom.lastIndexOf(")"));
			//system.out.println("inner axiom: "+innerAxiom);
			
			pattern.addAll(analyse(axiomType,innerAxiom));
			
		}else{
			subjectClass = axiom.substring(axiom.indexOf("#")+1,axiom.length()-1);
			s = subjectClass.split("-");
			//system.out.println("subjectClass2: "+subjectClass);
			
			for (String i: s){
				Word word = new Word(i);
				word.deriveType();
				if (word.getType()==Type.verb){
					word.setPattern(Pattern.sConcord);
				}
				pattern.add(word);
			}
		}
		
		/* Insert the kuphela last */
		Word kuphela = new Word("kuphela");
		kuphela.setStem("kuphela");
		kuphela.setPattern(Pattern.only);
		pattern.add(kuphela);
		
		return pattern;
	}

}
