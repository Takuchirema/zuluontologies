/** This class takes in an ArrayList of words and verbalizes them.
 * This is done using the verbalizing rules created
 * Pattern would be something like:
 * <noun> <verb> <noun>
 * <noun> <adjective>*/

import java.util.ArrayList;


public class WordsPattern {
	
	public String verbalized="";
	private ArrayList<Word> words = new ArrayList<Word>();
	
	public WordsPattern(ArrayList<Word> words){
		this.words=words;
	}
	
	public String verbalise(){
		
		String sentence="";
		
		Word quantitative = null;
		
		Word subject = new Word(null);
		
		try{
			subject = words.get(0);
		}catch(Exception e){
			
		}
		
		for (int i=0;i<words.size();i++){
			
			Word word=words.get(i);
			
			Pattern pattern = word.getPattern();
			
			String verbal;
			
			switch(pattern){
			
				case allQPronoun:
					verbal = vAllQPronoun(i,quantitative,word,subject);
					quantitative = word;
					sentence = sentence+" "+verbal;
					break;
				case oneQPronoun:
					verbal = vOneQPronoun(i,quantitative,word,subject);
					sentence = sentence+" "+verbal;
					break;
				case plural:
					break;
				case sConcord:
					verbal = vSConcord(i,quantitative,word,subject);
					sentence = sentence+" "+verbal;
					break;
				case root:
					break;
				case atLeast:
					break;
				case complement:
					verbal = vComplement(i,quantitative,word,subject);
					sentence = sentence+" "+verbal;
					break;
				case intersection:
					verbal = vIntersection(i,quantitative,word,subject);
					sentence = sentence+" "+verbal;
					break;
				case union:
					verbal = vUnion(i,quantitative,word,subject);
					sentence = sentence+" "+verbal;
					break;
				case only:
					verbal = vOnly(i,quantitative,word,subject);
					sentence = sentence+" "+verbal;
					break;
				case unknown:
					verbal = vUnknown(i,quantitative,word,subject);
					sentence = sentence+" "+verbal;
					break;
			}
	
		}
		
		System.out.println("--------- Sentence -----------");
		System.out.println(sentence);
		System.out.println("---------    End   -----------");
		
		return verbalized;
	}
	
	public String vAllQPronoun(int pos,Word quantitative,Word word,Word subject){
		String verbal = "";
		
		//System.out.println("verbalising: "+word.getWord());
		Word prev = words.get(pos-1);
		
		if (prev.getType()==Type.noun && !prev.getNounClass().isPluralClass){
			String strPluralClass = prev.getNounClass().getPluralClass();
			NounClass pluralClass = Test.getNounClasses().get(strPluralClass);
			word.setNounClass(pluralClass);
			
			String[] prefixes = pluralClass.getqPron().split(",");
			verbal = prefixes[0]+word.getStem();
		}else if (quantitative !=null){
			NounClass pluralClass = quantitative.getNounClass();
			word.setNounClass(pluralClass);
			//System.out.println("pnc: "+pluralClass.getClassName());
			String[] prefixes = pluralClass.getqPron().split(",");
			verbal = prefixes[0]+word.getStem();
		}else{
			String strPluralClass = subject.getNounClass().getPluralClass();
			NounClass pluralClass = Test.getNounClasses().get(strPluralClass);
			word.setNounClass(pluralClass);
			
			String[] prefixes = pluralClass.getqPron().split(",");
			verbal = prefixes[0]+word.getStem();
		}
		
		System.out.println("word: "+word.getWord()+" verbal: "+verbal);
		return verbal;
	}
	
	/** The previous word is always a noun of some sort
	 * e.g. inja ye-dwa */
	public String vOneQPronoun(int pos,Word quantitative,Word word,Word subject){
		String verbal = "";
		
		Word prev = words.get(pos-1);
		
		if (prev.getType()==Type.noun){
			if(prev.getNounClass().isPluralClass())
				verbal=prev.getNounClass().getOneConc()+"nye";
			else
				verbal=prev.getNounClass().getOneConc()+"dwa";
		}
		
		System.out.println("word: "+word.getWord()+" verbal: "+verbal);
		return verbal;
	}
	
	/** The Verb is conjugated with the quantitative noun class */
	public String vSConcord(int pos,Word quantitative,Word word,Word subject){
		String verbal = "";
		
		verbal=quantitative.getNounClass().getsConc()+word.getStem();
		
		System.out.println("word: "+word.getWord()+" verbal: "+verbal);
		return verbal;
	}
	
	public String vComplement(int pos,Word quantitative,Word word,Word subject){
		String verbal = "";
		
		Word next = words.get(pos+1);
		
		if (next.getType()==Type.noun)
			verbal="aku"+next.getNounClass().getaPron();
		
		System.out.println("word: "+word.getWord()+" verbal: "+verbal);
		return verbal;
	}
	
	public String vIntersection(int pos,Word quantitative,Word word,Word subject){
		String verbal = "";
		
		verbal=word.getWord();
		
		System.out.println("word: "+word.getWord()+" verbal: "+verbal);
		return verbal;
	}
	
	public String vUnion(int pos,Word quantitative,Word word,Word subject){
		String verbal = "";
		
		verbal=word.getWord();
		
		System.out.println("word: "+word.getWord()+" verbal: "+verbal);
		return verbal;
	}
	
	public String vOnly(int pos,Word quantitative,Word word,Word subject){
		String verbal = "";
		
		verbal=word.getWord();
		
		System.out.println("word: "+word.getWord()+" verbal: "+verbal);
		return verbal;
	}
	
	public String vUnknown(int pos,Word quantitative,Word word,Word subject){
		String verbal = "";
		
		Word prev;
		if (word.getType()==Type.noun && pos!=0 && words.size()>2){
			prev = words.get(pos-1);
			if(prev.getPattern()==Pattern.allQPronoun){
				verbal=prev.getNounClass().getsConc()+"yi"+word.getStem();
			}else if(prev.getType()==Type.noun){
				/** This is from page 20 of ZuluLessons in literature folder 
				 * Explanation is in notes */
				String pConc=word.getNounClass().getpConc().split(",")[0];
				//char possesiveVowel = pConc.charAt(pConc.length()-1);
				char prefixVowel = word.getNounClass().getPrefix().charAt(0);
				
				if (prefixVowel=='u'){
					verbal=pConc.substring(0,pConc.length()-1)+"o"+word.getWord().substring(1,word.getWord().length());
				}else if (prefixVowel=='i'){
					verbal=pConc.substring(0,pConc.length()-1)+"e"+word.getWord().substring(1,word.getWord().length());
				}else if (prefixVowel=='a'){
					verbal=pConc.substring(0,pConc.length()-1)+"a"+word.getWord().substring(1,word.getWord().length());
				}
				
			}
		}
		
		if (!verbal.equals(""))
			System.out.println("word: "+word.getWord()+" verbal: "+verbal);
		else{
			verbal=word.getWord();
			System.out.println("UP word: "+word.getWord()+" verbal: "+verbal);
		}
		
		return verbal;
	}

}
