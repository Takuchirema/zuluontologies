import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class Word {
	
	private  String word="";
	private  String stem="";
	private  String root="";
	private  NounClass nounClass;
	
	private Type type;
	private MetaData metaData = new MetaData();
	
	private Pattern pattern=Pattern.unknown;;
	
	public  void main(String[] args) throws IOException{
		word="okumnandi";
		
		deriveType();
		//System.out.println("root: "+getVerbRoot(word));
	}
	
	public Word(String word,Type type){
		this.word=word;
		this.type=type;
	}
	
	public Word(String word){
		this.word=word;
	}
	
	/** This method checks which type the word is
	 * If its noun then it references the noun class
	 * It can be a verb
	 * It can be an adjective 
	 * @throws IOException */
	public  void deriveType() throws IOException{
		
		HashMap<String,String> savedNouns = Test.loadSavedNouns();
		HashMap<String,NounClass> nounClasses = Test.loadNounClasses();
		HashMap<Character,ArrayList<String>> verbLists = Test.loadVerbs();
		ArrayList<String> adjectives = Test.loadAdjectives();
		
		if (savedNouns.containsKey(word)){
			type = Type.noun;
			nounClass = nounClasses.get(savedNouns.get(word));
			stem=nounClass.getWordStem(word);
			System.out.println(word+" is: noun. Class is: "+nounClass.getClassName());
		}else if (isInVerbs(verbLists)){
			type = Type.verb;
			stem=word;
			System.out.println(word+" is: verb");
		}else if (isVerb(word,verbLists)){
			/** Stem is set by the method */
			type = Type.verb;
			System.out.println(word+" is: verb by derivation");
		}else if (adjectives.contains(word)){
			type = Type.adjective;
			stem=word;
			System.out.println(word+" is: adjective");
		}else if (isAdjective(word,adjectives)){
			/** Stem is set by the method */
			type = Type.adjective;
			System.out.println(word+" is: adjective by derivation");
		}else if (isNoun(word,nounClasses)){
			/** Now we at last, check if it is a noun by derivation
			 * Stem is set by the method */
			type = Type.noun;
			System.out.println(word+" is: noun by derivation. Class is: "+nounClass.getClassName());
		}else{
			type = Type.unknown;
			System.out.println("Type not found for "+word);
		}
	}
	
	public  boolean isInVerbs(HashMap<Character,ArrayList<String>> vl){
		boolean isVerb = false;
		ArrayList<Character> used = new ArrayList<Character>();
		
		for (int i=0;i<word.length();i++){
			if (!used.contains(word.charAt(i))){
				ArrayList<String> verbs = vl.get(word.charAt(i));
				used.add(word.charAt(i));
				if (verbs != null && verbs.contains(word))
					isVerb=true;
			}
		}
		
		return isVerb;
	}
	
	/** Assuming the word is a verb, it must be a 100% match with one verb
	 * This is when the root of the word matches the root of one of the verbs */
	public  boolean isVerb(String word,HashMap<Character,ArrayList<String>> verbLists){
		
		boolean isVerb = false;
		
		//System.out.println("not clear, check if verb");
		ArrayList<Character> visited = new ArrayList<Character>();
		
		outerloop:
		for (int i=0;i<word.length();i++){
			if(!visited.contains(word.charAt(i))){
				visited.add(word.charAt(i));
				ArrayList<String> verbList = verbLists.get(word.charAt(i));
				
				if (verbList != null){
					for (String verb: verbList){
						String verbRoot = getVerbRoot(verb);
						String root = getVerbRoot(word);
						
						int score1 = Test.distance(root, verbRoot);
						int score2 = Test.distance(root, verb);
						
						//System.out.println(root+" , "+verbRoot+" score1: "+score1+" score2: "+score2);
						if (score1 == 0 || score2 == 0){
							isVerb = true;
							stem=verb;
							break outerloop;
						}
					}
				}
				
			}
		}
		
		return isVerb;
		
	}
	
	/** Checks if the word is an adjective
	 * This is done by comparing the root of the word to every adjective */
	public  boolean isAdjective(String word,ArrayList<String> adjectives){
		
		boolean isAdj = false;
		
		//System.out.println("not clear, check if adjective");
		
		loop:
		for (String adjective: adjectives){
			String adjRoot = getVerbRoot(adjective);
			String root = getVerbRoot(word);
			
			int score = Test.distance(root, adjRoot);
			int score2 = Test.distance(root, adjective);
			
			//System.out.println(root+" , "+adjRoot+": "+score);
			if (score == 0 || score2 ==0){
				System.out.println(root+" , "+adjRoot+": "+score);
				isAdj = true;
				stem=adjective;
				break loop;
			}
		}
		return isAdj;
	}
	
	public  boolean isNoun(String word, HashMap<String,NounClass> nounClasses){
		
		boolean isNoun = false;
		
		String prefix = "";
		int prefixLength = 0;
		
		int cut = Math.min(word.length(), 3);
		
		for (int i=cut;i>=0;i--){
			prefix = word.substring(0,i);
			//System.out.println("checking prefix: "+prefix);
			
			for (Map.Entry<String, NounClass> e:nounClasses.entrySet()){
				String[] prefixes = e.getValue().getPrefix().split(",");
				
				//System.out.println("checking prefs: "+Arrays.toString(prefixes));
				if (Arrays.asList(prefixes).contains(prefix)){
					//System.out.println("Noun Class found: "+e.getValue().className);
					isNoun = true;
					
					if (prefix.length() > prefixLength){
						nounClass = e.getValue();
						stem=nounClass.getWordStem(word);
						prefixLength = prefix.length();
					}
				}
				
			}
		}
		
		return isNoun;
	}
	
	
	/** This class gets the transformed word in locative form
	 * e.g. umfula -> emfuleni */
	public  String deriveLocative(){
		
		String derived="";
		char[] wordChar = word.toCharArray();
		int lastPos = wordChar.length -1;
		char lastChar = wordChar[lastPos];
		char secLastChar = wordChar[lastPos-1];
		
		if (type.equals("propername")){
			if (word.charAt(0)=='u')
				derived="ku"+word.substring(1,word.length()-1);
			else
				derived="ku"+word;
		}else if (lastChar == 'a')
			derived = consonantLocative(secLastChar,word,"eni");
		else if (lastChar == 'e')
			derived = consonantLocative(secLastChar,word,"eni");
		else if (lastChar == 'i')
			derived = consonantLocative(secLastChar,word,"ini");
		else if (lastChar == 'o')
			derived = consonantLocative(secLastChar,word,"weni");
		else if (lastChar == 'u')
			derived = consonantLocative(secLastChar,word,"wini");
		
		return derived;
	}
	
	public  String consonantLocative(char cons,String word,String append){
		
		int lastPos = word.length() -1;
		String derived="";
		
		if (cons == 'v' || cons == 'f')
			derived = "e"+word.substring(1,lastPos)+append.replace("w", "");
		else if (cons == 'b'){
			if (word.charAt(lastPos-2)=='m'){
				derived = "e"+word.substring(1,lastPos-2)+"nj"+append.replace("w", "");
			}else
				derived = "e"+word.substring(1,lastPos-1)+"tsh"+append.replace("w", "");
		}else if (cons == 'p'){
			derived = "e"+word.substring(1,lastPos-1)+"tsh"+append.replace("w", "");
		}else if (cons == 'm'){
			derived = "e"+word.substring(1,lastPos-1)+"ny"+append.replace("w", "");
		}else{
			derived = "e"+word.substring(1,lastPos)+append;
		}
		
		return derived;
	}
	
	/** Derives the passive of a verb
	 * e.g. thanda -> thandwa 
	 * Rules
	 * If the Root has now vowels then put -iwa
	 * e.g. -dl- -> dliwa
	 * If Root has vowels then check syllables OTHER THAN THE FIRST to see if they have
	 * b, p, m or mb
	 * b -> tsh, p -> sh, m -> ny, mb -> nj
	 * e.g. bamba -> ba nj wa*/
	public  String derivePassive(){
		
		ArrayList<Character> vowels = new ArrayList<Character>();
		vowels.add('a');vowels.add('e');vowels.add('i');vowels.add('o');vowels.add('u');
		
		String derived="";
		String root;
		String[] syllables;
		
		if (vowels.contains(word.charAt(0)) ){
			syllables = word.substring(1,word.length()-1).split("a|e|i|o|u");
			root = word.substring(1,word.length()-1);
		}else{
			syllables = word.substring(1,word.length()-1).split("a|e|i|o|u");
			root = word.substring(0,word.length()-1);
		}
		
		System.out.println("word Length: "+syllables.length+" Root: "+root);
		System.out.println("root split: "+Arrays.toString(root.split("a|e|i|o|u")) );
		if (root.split("a|e|i|o|u").length == 1){
			derived = word.substring(0,word.length()-1)+"iwa";
		}else{
			int sCount = 0;
			for (int i=0;i<word.length();i++){
				
				if (vowels.contains(word.charAt(i)) ){
					
					if (sCount > 0){
						
						if (word.charAt(i-1)=='b' && i>0 && word.charAt(i-2)=='m' ){
							derived = derived.substring(0,derived.length()-2)+"nj"+word.charAt(i); 
						}else if (word.charAt(i-1)=='b'){
							//System.out.println("got a b "+derived);
							derived = derived.substring(0,derived.length()-2)+"tsh"+word.charAt(i);  
						}else if (word.charAt(i-1)=='p'){
							derived = derived.substring(0,derived.length()-2)+"sh"+word.charAt(i); 
						}else if (word.charAt(i-1)=='m'){
							derived = derived.substring(0,derived.length()-2)+"ny"+word.charAt(i); 
						}else{
							derived = derived + word.charAt(i);
						}
					}else{
						derived = derived + word.charAt(i);
						sCount++;
					}
					
					if (i==word.length()-1){
						derived = derived.substring(0,derived.length()-1)+"w"+word.charAt(word.length()-1);
					}
					
				}else{
					derived=derived+word.charAt(i);
				}
			}
			
		}
		
		return derived;
	}
	
	/** Gets the 'Safe' Root of a verb. Something that does not start with a vowel
	 * Rules:
	 * Remove the first vowel
	 * Remove the first syllable iff there is more than one.
	 * e.g. ukuhamba -> hamba, dla -> dla, idla -> dla etc*/
	public  String getVerbRoot(String word){
		String root = word;
		ArrayList<Character> vowels = new ArrayList<Character>();
		vowels.add('a');vowels.add('e');vowels.add('i');vowels.add('o');vowels.add('u');
		int sCount =0;
		
		if (vowels.contains(word.charAt(0)) ){
			root = word.substring(1,word.length());
		}
		
		sCount = word.split("a|e|i|o|u").length;
		//System.out.println("root: "+root+" "+Arrays.toString(word.split("a|e|i|o|u")));
		if (sCount > 2){
			loop:
			for (int i=0;i<root.length();i++){
				if (vowels.contains(root.charAt(i))){
					root=root.substring(i+1,root.length());
					break loop;
				}
			}
		}
		
		
		return root;
	}
	
	
	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public MetaData getMetaData() {
		return metaData;
	}

	public void setMetaData(MetaData metaData) {
		this.metaData = metaData;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getStem() {
		return stem;
	}

	public void setStem(String stem) {
		this.stem = stem;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}
	
	public  NounClass getNounClass() {
		return nounClass;
	}

	public  void setNounClass(NounClass nounClass) {
		this.nounClass = nounClass;
	}
	

}
