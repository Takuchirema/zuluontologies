import java.util.ArrayList;


public class NounClass {
	
	/** Class	Prefix	Adj	PConc	SConc	OConc	QPron	NConc	PluralClass 
	 * Adj -> Adjective Concordance
	 * SConc -> Subject Concordance
	 * OConc -> Object Concordance
	 * QPron -> Quantitative Pronoun
	 * NConc -> Negative Concordance
	 * OneConc -> One Concodence e.g. umuntu oyedwa
	 * APron -> Absolute Pronoun. Used in Negative e.g. Akulona igatsha -> not a branch
	 * PluralClass -> Class which pluralizes the current one*/
	
	String className;
	String prefix;
	String adj;
	String pConc;
	String sConc;
	String oConc;
	String qPron;
	String nConc;
	String oneConc;
	String aPron;
	String pluralClass;
	
	
	/* we will not use these for now
	String OConc;
	String Adj;
	*/
	
	boolean isPluralClass = true;
	
	
	public NounClass(String className){
		this.className=className;
	}
	
	public void setVariables(String[] vars){
		
		for (int i=0;i<vars.length;i++){
			String var = vars[i];
			
			switch (i){
				case 1:
					if (className.equals("9"))
						prefix="i";
					else
						prefix = var;
					break;
				case 2:
					adj = var;
					break;
				case 3:
					pConc = var;
					break;
				case 4:
					sConc = var;
					break;
				case 5:
					oConc = var;
					break;
				case 6:
					qPron = var;
					break;
				case 7:
					nConc = var;
					break;
				case 8:
					oneConc = var;
					break;
				case 9:
					aPron = var;
					break;
				case 10:
					pluralClass =var;
					isPluralClass=false;
					break;
			}
			
		}
		
	}
	
	/** Derives the root of a word belonging to the nounclass 
	 * e.g. isilwane -> lwane*/
	public String getWordStem(String word){
		String stem="";
		
		String[] prefixes = prefix.split(",");
		int usedPrefixLength=0;
		
		for (String pref:prefixes){
			int l = pref.length();
			
			if (word.substring(0, l).equals(pref) && (l>usedPrefixLength)){
				usedPrefixLength=l;
			}
				
		}
		
		stem=word.substring(usedPrefixLength,word.length());
		//System.out.println("in nc getstem stem: "+stem);
		
		return stem;
	}

	
	
	public String getAdj() {
		return adj;
	}

	public void setAdj(String adj) {
		this.adj = adj;
	}

	public String getpConc() {
		return pConc;
	}

	public void setpConc(String pConc) {
		this.pConc = pConc;
	}

	public String getsConc() {
		return sConc;
	}

	public void setsConc(String sConc) {
		this.sConc = sConc;
	}

	public String getoConc() {
		return oConc;
	}

	public void setoConc(String oConc) {
		this.oConc = oConc;
	}

	public String getqPron() {
		return qPron;
	}

	public void setqConc(String qPron) {
		this.qPron = qPron;
	}

	public String getnConc() {
		return nConc;
	}

	public void setnConc(String nConc) {
		this.nConc = nConc;
	}

	public String getOneConc() {
		return oneConc;
	}

	public void setOneConc(String oneConc) {
		this.oneConc = oneConc;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPluralClass() {
		return pluralClass;
	}

	public void setPluralClass(String pluralClass) {
		this.pluralClass = pluralClass;
	}

	public boolean isPluralClass() {
		return isPluralClass;
	}

	public void setPluralClass(boolean isPluralClass) {
		this.isPluralClass = isPluralClass;
	}

	public String getaPron() {
		return aPron;
	}

	public void setaPron(String aPron) {
		this.aPron = aPron;
	}
	

}
