
public enum MetaType {
	
	role,
	entityType,
	readingPattern,
	axiomType,
	reading;

}
