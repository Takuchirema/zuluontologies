/** In Zulu, the noun is composed of two formative
 * Stem and Prefix*/

public class Noun {
	
	private String word;
	private String stem;
	private String prefix;
	
	public Noun(String stem,String prefix,String word){
		this.word=word;
		this.stem=stem;
		this.prefix=prefix;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getStem() {
		return stem;
	}

	public void setStem(String stem) {
		this.stem = stem;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

}
